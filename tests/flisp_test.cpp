#include "doctest.h"
#include "flisp.hpp"
#include <iostream>


TEST_CASE("pad test")
{
    REQUIRE(padTokens("(") == " ( ");
    REQUIRE(padTokens(")") == " ) ");
    REQUIRE(padTokens("(1)") == " ( 1 ) ");
    REQUIRE(padTokens("(\"    \")") == " ( \"    \" ) ");
    REQUIRE(padTokens("(())") == " (  (  )  ) ");
    REQUIRE(padTokens("2345") == "2345 ");
    REQUIRE(padTokens("1 2 3 4 5") == "1 2 3 4 5 ");
    REQUIRE(padTokens("1  2  3  ") == "1  2  3  ");
}

TEST_CASE("strip test")
{
    REQUIRE(stripWhitespace("") == "");
    REQUIRE(stripWhitespace(" ") == " ");
    REQUIRE(stripWhitespace("   ") == " ");
    REQUIRE(stripWhitespace("        ") == " ");
    REQUIRE(stripWhitespace("  1  2  3  ") == " 1 2 3 ");
    REQUIRE(stripWhitespace("\"    \"") == "\"    \"");
    REQUIRE(stripWhitespace("\"1234\"") == "\"1234\"");
    REQUIRE(stripWhitespace("a   \"1234\"   b") == "a \"1234\" b");
}


TEST_CASE("tokenize tests")
{
    auto a = tokenize(stripWhitespace(padTokens(" ( ")));
    REQUIRE(a[0] == "(");
    REQUIRE(a.size() == 1);

    auto b = tokenize(stripWhitespace(padTokens(" ) ")));
    REQUIRE(b[0] == ")");
    REQUIRE(b.size() == 1);

    auto c = tokenize(stripWhitespace(padTokens("((((")));
    REQUIRE(c.size() == 4);
    REQUIRE(c[0] == "(");
    REQUIRE(c[1] == "(");
    REQUIRE(c[2] == "(");
    REQUIRE(c[3] == "(");

    auto d = tokenize(stripWhitespace(padTokens("))))")));
    REQUIRE(d.size() == 4);
    REQUIRE(d[0] == ")");
    REQUIRE(d[1] == ")");
    REQUIRE(d[2] == ")");
    REQUIRE(d[3] == ")");

    REQUIRE(tokenize(stripWhitespace(padTokens("\"as  df\"")))[0] == "\"as  df\"");
    REQUIRE(tokenize(stripWhitespace(padTokens("\"asdf\"")))[0] == "\"asdf\"");
    REQUIRE(tokenize(stripWhitespace(padTokens("asdf")))[0] == "asdf");
    REQUIRE(tokenize(stripWhitespace(padTokens("as\ndf")))[0] == "as");
    REQUIRE(tokenize(stripWhitespace(padTokens("as\ndf")))[1] == "df");
    REQUIRE(tokenize(stripWhitespace(padTokens("as   df")))[0] == "as");
    REQUIRE(tokenize(stripWhitespace(padTokens("as   df")))[1] == "df");
    {
        std::vector<std::string> tokens;
        tokens.push_back("(");
        tokens.push_back(")");
        auto temp = tokenize("()");
        REQUIRE(tokens.size() == temp.size());
        for(auto i=0U; i<tokens.size(); ++i)
            REQUIRE(temp[i] == tokens[i]);
    }
}

