/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#pragma once

#include <functional>
#include <memory>
#include <utility>
#include <string>


enum class SCMType{
    Nil,
    Boolean,
    Pair,
    Symbol,
    Fixnum,
    Flonum,
    Character,
    String,
    Vector,
    Port,
    Procedure,
};


class SCMObj{

private:
    SCMType type;

    char ch;
    int64_t fixnum;
    double flonum;
    bool b;
    std::string str;
    std::string sym;
    std::function<SCMObj(const SCMObj&)> proc;
    std::unique_ptr<SCMObj> first;
    std::unique_ptr<SCMObj> rest;

    bool isError;
    std::unique_ptr<std::pair<std::string, std::string>> errorData;

public:
    SCMObj();
    SCMObj(const SCMObj& other);
    SCMObj(SCMObj&& other);
    SCMObj& operator=(const SCMObj& other) = delete;
    SCMObj& operator=(SCMObj&& other) = delete;
    ~SCMObj();

    std::string toString() const;
    SCMType getType() const;
    std::string getTypeAsString() const;

    bool isString() const;
    bool isNumber() const;
    bool isBoolean() const;
    bool isCharacter() const;
    bool isSymbol() const;
    bool isSelfEvaluating() const;
    bool isPair() const;
    bool isTrue() const;
    bool isFalse() const;
    bool isIf() const;
    bool isAtom() const;

    double getFlonum() const;
    void setString(const std::string&);
    void setFixnum(uint64_t);
    void setFlonum(double);
    void setBoolean(bool);
    void setCharacter(char);
    void setCharacter(const std::string&);
    void setSymbol(const std::string&);
    void setPair(const SCMObj&, const SCMObj&);
    void setProcedure(std::function<SCMObj(SCMObj)>);
    void setError(const std::string&, const std::string&);
    void setBuiltin(bool b = true);
    void setIf();

    SCMObj car() const;
    SCMObj cdr() const;
    bool isTaggedList(const std::string&) const;


};

