/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <limits>
#include <exception>
#include <cassert>
#include "env.hpp"
#include "scmobj.hpp"
#include <vector>


SCMObj eval(const SCMObj&, const Env*);

//constructs scheme objects from token container
SCMObj parseTokens(std::vector<std::string> tokens);

//returns a container of tokens formed from the expression string
std::vector<std::string> tokenize(const std::string& str);

//removes extra whitespace
std::string stripWhitespace(const std::string&);

//adds extra whitespace between tokens to separate them better
std::string padTokens(const std::string&);

//reads from console in
SCMObj readCin();

