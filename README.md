FLisp
====

This project implements a scheme lisp dialect for learning purposes. 

(Do Not Use - slow and inefficient! - I recommend Chicken Scheme, Gambit Scheme, 
Clozure CL, or SBCL if you want a real lisp)

Use `build.sh` to generate makefiles and `make` debug and release binaries

Or open `flisp.cbp` with Code::Blocks.

Or if you know how to use CMake, you could use another generator.

---

Made with ideas from:

http://norvig.com/lispy.html

http://norvig.com/lispy2.html

http://www.michaelnielsen.org/ddi/lisp-as-the-maxwells-equations-of-software/

http://peter.michaux.ca/articles/scheme-from-scratch-introduction
