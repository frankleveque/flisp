/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <memory>
#include <limits>
#include <exception>
#include <env.hpp>
#include <scmobj.hpp>
#include <cassert>


// the lisp evaluator
SCMObj eval(const SCMObj& exp, const Env* env)
{
    if(exp.isSelfEvaluating()) {
        return exp;
    } else if(exp.isSymbol()) {
        auto sym = env->find(exp.toString());
        if(sym != env->end()) {
            return *(sym->second);
        } else {
            SCMObj temp;
            temp.setError("Symbol not found in environment", exp.toString());
            return temp;
        }
    }
    SCMObj ret;
    ret.setError("EvalError - Unknown Expression Type", exp.toString());
    return ret;
}

/*
(define apply (proc args)
 (if (primitive? proc)
      (do-magic proc args)
      (eval (substitute (body proc) (formals proc args)))))
*/
/*
(define apply-1 (proc args)
    (cond ((procedure? proc)
           (apply proc args))
          (lambda-exp? proc)
           (eval-1 (substitute (caddr proc) ;body
                               (cadr proc)  ;formal parameters
                               args         ;actual args
                               '())))       ;bound-vars
)
*/





SCMObj parseTokens(std::vector<std::string> tokens)
{
    SCMObj temp;

    if(tokens.size() > 0) {
        std::string first = tokens.front();
        if(first == "(") {      // scheme list
            temp.setPair(SCMObj(), SCMObj());
            //recursive list processing here
        } else if(first[0] == '#') {         //char
            if(first == "#t" || first == "#T") {
                temp.setBoolean(true);
            } else if(first == "#f" || first == "#F") {
                temp.setBoolean(false);
            } else if(first[1] == '\\') {
                temp.setCharacter(first.substr(2));
            } else {
                temp.setError("InvalidChar", first);
            }
        } else if(first[0] == '"') {      //string
            std::stringstream ss;
            for(auto t : tokens) {
                auto i = 0U;
                while(i < t.size()) {
                    if(t[i] == '\\' && t[i+1] == 'n') {
                        ss << '\n';
                        i+=2;
                    } else if(t[i] == '"') {
                        i++;
                    } else {
                        ss << t[i];
                        i++;
                    }
                }
            }
            temp.setString(ss.str());
        } else {
            try {
                if(first.find('.') != first.npos) {
                    temp.setFlonum(std::stod(first));
                } else {
                    temp.setFixnum(std::stoi(first));
                }
            } catch(const std::invalid_argument& e) {
                temp.setSymbol(first);
            }
        }
    }

    return temp;
}



std::vector<std::string> tokenize(const std::string& str)
{
    std::vector<std::string> tokens;
    std::stringstream ss;
    bool in_string = false;


    for(auto it=str.cbegin(); it!=str.cend(); ++it) {
        if(*it == '"') {
            in_string = !in_string;
            if(ss.str().size() > 0) {
                ss << *it;
                tokens.push_back(ss.str());
                ss.str("");
            } else {
                ss << *it;
            }
        } else if(in_string) {
            ss << *it;
        } else {
            if(*it == '(' || *it == ')') { //parens
                if(ss.str().size() > 0) {
                    tokens.push_back(ss.str());
                    ss.str("");
                }
                ss << *it;
                tokens.push_back(ss.str());
                ss.str("");
            } else if(*it != ' ') { //not whitespace
                ss << *it;
            } else { //whitespace
                if(ss.str().size() > 0) {
                    tokens.push_back(ss.str());
                    ss.str("");
                }
            }
        }
        if(it == str.cend() && ss.str().size() > 0) {
            tokens.push_back(ss.str());
        }

    }
    return tokens;
}

std::string stripWhitespace(const std::string& str)
{
    std::stringstream ss;
    bool in_string = false;

    for(auto it = str.begin(); it != str.end(); ) {
        if(*it == '"') {
            in_string = !in_string;
            ss << *it++;
        } else if(in_string) {
            ss << *it++;
        } else if(!in_string) {
            if(*it != ' ') {
                ss << *it++;
            } else if(*it == ' ' && ss.str().back() == ' ') {
                it++;
            } else {
                ss << *it++;
            }
        }
    }
    return ss.str();
}


std::string padTokens(const std::string& str)
{
    std::stringstream ss;
    bool in_string = false;

    for(auto c : str) {
        if(c == '"') {
            in_string = !in_string;
            ss << c;
        } else if(in_string) {
            ss << c;
        } else if(c == '\t' || c == '\r' || c == '\n') {
            ss << ' ';
        } else if(c == '(' || c == ')') {
            if(in_string) {
                ss << c ;
            } else {
                ss << ' ' << c << ' ';
            }
        } else {
            ss << c;
        }
    }
    if(ss.str().back() != ' ') {
        ss << ' ';
    }
    return ss.str();
}

//read from standard input
SCMObj readCin()
{
    std::string line;
    std::getline(std::cin, line);

    auto tokens = tokenize(stripWhitespace(padTokens(line)));
    if(tokens.size() == 0) {
        SCMObj temp;
        temp.setError("empty token list", "''");
        return temp;
    }
    return parseTokens(tokens);
}


