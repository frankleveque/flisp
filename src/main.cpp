/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include <flisp.hpp>

int main(int argc, char* argv[])
{
    if(argc > 1) {
        //read file(s) from console args and eval here
        std::cout << "Commandline Args: " << std::endl;
        for(auto i=0; i<argc; ++i) {
            std::cout << "    " << argv[i] << std::endl;
        }
        std::cout << std::endl;
    }
    std::unique_ptr<Env> globalEnv = Env::globalEnv();

    auto t = tokenize(stripWhitespace(padTokens("32 77")));
    for(auto k : t){
        std::cout << k << std::endl;
    }

    //(loop (print (eval (read))))
    while(true) {
        std::cout << "flisp> ";
        std::cout << eval(readCin(), globalEnv.get()).toString() << std::endl;
    }
}

