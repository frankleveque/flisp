/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "env.hpp"

#include <cassert>
#include <iostream>

Env::Env()
{

}

Env::Env(Env& parent)
{
    this->outer.reset(&parent);
}

std::unordered_map<std::string, std::unique_ptr<SCMObj>>::const_iterator Env::find(const std::string& var) const
{
    return dict.find(var);
    /*
    auto search = dict.find(var);
    if(search != dict.end()){
       return *search->second.get();
    }else{
        if(outer){
            return outer->find(var);
        }else{
            auto temp = std::unique_ptr<SCMObj>(new SCMObj);
            temp->setError("Symbol not found in environment", var);
            return *temp.get();
        }
    }*/
}

std::unordered_map<std::string, std::unique_ptr<SCMObj>>::const_iterator Env::begin() const
{
    return dict.begin();

}

std::unordered_map<std::string, std::unique_ptr<SCMObj>>::const_iterator Env::end() const
{
    return dict.end();

}

void Env::add(const std::string& name, std::unique_ptr<SCMObj> val)
{
    this->dict[name] = std::move(val);
}


std::unique_ptr<Env> Env::globalEnv()
{
    std::unique_ptr<Env> global (new Env());


    auto testStr = std::unique_ptr<SCMObj>(new SCMObj);
    testStr->setString("asdf");
    global->add(std::string("test-str"), std::move(testStr));

    auto testNum = std::unique_ptr<SCMObj>(new SCMObj);
    testNum->setFixnum(9999);
    global->add(std::string("test-num"), std::move(testNum));

    auto testBool = std::unique_ptr<SCMObj>(new SCMObj);
    testBool->setBoolean(false);
    global->add(std::string("test-bool"), std::move(testBool));

    /*
    auto builtin_add = std::unique_ptr<SCMObj>(new SCMObj);
    builtin_add->setProcedure(
            [](SCMObj args){
            //SCMObj* current = &args;
            //while(!current->isFalse()){
            //    result += current->car().getNumber();
            //    current = &current->cdr();
            //}
            auto result = std::unique_ptr<SCMObj>()->setError("Not implemented yet", "None");
            return result;
            }).setBuiltin();

    global->add(std::string("add"), std::mabuiltin_add);
    */
    /*
    auto builtin_print_globals = std::unique_ptr<SCMObj>(new SCMObj);
    builtin_print_globals->setProcedure(
            [&](SCMObj args){
            for(auto it = global->dict.cbegin(); it != global->dict.cend(); ++it){
                auto lenfirst = it->first.size();
                auto lensec = it->second->getTypeAsString().size();
                auto spacing = 20U;
                std::cout << it->first;
                if(lenfirst <= spacing){
                    for(uint32_t i = 0U; i<(20 - lenfirst); ++i){
                        std::cout << " ";
                    }
                }
                std::cout << ":  " << it->second->getTypeAsString();
                if(lensec <= spacing){
                    for(uint32_t i = 0U; i<(20 - lensec); ++i){
                        std::cout << " ";
                    }
                }
                std::cout << std::endl;
            }
            auto result = std::unique_ptr<SCMObj>(new SCMObj)->setBoolean(true);
            return result;
            }).setBuiltin();
            */

    //global->add(std::string("print-globals"), std::move(builtin_print_globals));



    return global;
}

