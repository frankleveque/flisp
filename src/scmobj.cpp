/*
FLisp - Scheme interpreter implemented in C++1x
===============================================================================
Copyright (C) 2017 Frank Leveque

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "scmobj.hpp"
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>

SCMObj::SCMObj()
{
    this->type = SCMType::Nil;
    /*
    this->ch = 0;
    this->fixnum = 0;
    this->flonum = 0.0;
    this->b = false;
    this->str = "";
    this->sym = "";
    this->proc = nullptr;
    this->pair = std::make_pair(nullptr, nullptr);
    std::shared_ptr<SCMObj>(nullptr));
    std::shared_ptr<SCMObj>(nullptr));
    std::stringstream ss;
    ss << this;
    this->errorVal = ss.str();
    this->builtin = false;

 */
    std::stringstream ss;
    ss << std::hex << this;
    setError("Uninitialized Scheme Object", ss.str());
}

SCMObj::SCMObj(const SCMObj& other)
{
    this->type = other.type;
    this->isError = other.isError;
    this->errorData.reset(new std::pair<std::string, std::string>());
    this->errorData->first = other.errorData->first;
    this->errorData->second = other.errorData->second;

    switch(other.type) {
    case SCMType::String:
        this->str = other.str;
        break;
    case SCMType::Symbol:
        this->sym = other.sym;
        break;
    case SCMType::Fixnum:
        this->fixnum = other.fixnum;
        break;
    case SCMType::Flonum:
        this->flonum = other.flonum;
        break;
    case SCMType::Boolean:
        this->b = other.b;
        break;
    case SCMType::Character:
        this->ch = other.ch;
        break;
    case SCMType::Pair:
        break;
    case SCMType::Procedure:
        break;
    }
}

SCMObj::SCMObj(SCMObj&& other)
{
    this->type = std::move(other.type);
    this->isError = std::move(other.isError);
    this->errorData.reset(new std::pair<std::string, std::string>());
    this->errorData = std::move(other.errorData);

    switch(other.type) {
    case SCMType::String:
        this->str = std::move(other.str);
        break;
    case SCMType::Symbol:
        this->sym = std::move(other.sym);
        break;
    case SCMType::Fixnum:
        this->fixnum = std::move(other.fixnum);
        break;
    case SCMType::Flonum:
        this->flonum = std::move(other.flonum);
        break;
    case SCMType::Boolean:
        this->b = std::move(other.b);
        break;
    case SCMType::Character:
        this->ch = std::move(other.ch);
        break;
    case SCMType::Pair:
        break;
    case SCMType::Procedure:
        break;
    }
}

/*
this->type = std::move(other.type);
this->ch = std::move(other.ch);
this->num = std::move(other.num);
this->b = std::move(other.b);
this->str = std::move(other.str);
this->proc = std::move(other.proc);
//this->pair = std::move(other.pair);
this->sym = std::move(other.sym);
this->errorflag = std::move(other.errorflag);
this->errormsg = std::move(other.errormsg);
this->errorval = std::move(other.errorval);
this->builtin = std::move(other.builtin);

}

SCMObj& SCMObj::operator=(const SCMObj& other){

this->type = other.type;
this->ch = other.ch;
this->num = other.num;
this->b = other.b;
this->str = other.str;
this->proc = other.proc;
//this->pair = other.pair;
this->sym = other.sym;
this->isError = other.isError;
this->errorMsg = other.errorMsg;
this->errorVal = other.errorVal;
this->builtin = other.builtin;

int wat;
return *this;
}

SCMObj& SCMObj::operator=(SCMObj&& other){
this->type = std::move(other.type);
this->ch = std::move(other.ch);
this->num = std::move(other.num);
this->b = std::move(other.b);
this->str = std::move(other.str);
this->proc = std::move(other.proc);
this->pair = std::move(other.pair);
this->sym = std::move(other.sym);
this->isError = std::move(other.isError);
this->errorMsg = std::move(other.errorMsg);
this->errorVal = std::move(other.errorVal);
this->builtin = std::move(other.builtin);

return *this;
}
*/
SCMObj::~SCMObj()
{

}

bool SCMObj::isString() const
{
    return this->type == SCMType::String;
}

bool SCMObj::isNumber() const
{
    return (this->type == SCMType::Fixnum) ||
           (this->type == SCMType::Flonum);
}

bool SCMObj::isBoolean() const
{
    return this->type == SCMType::Boolean;
}

bool SCMObj::isCharacter() const
{
    return this->type == SCMType::Character;
}

bool SCMObj::isSymbol() const
{
    return this->type == SCMType::Symbol;
}

bool SCMObj::isSelfEvaluating() const
{
    return
        this->isBoolean() ||
        this->isNumber() ||
        this->isCharacter() ||
        this->isString();
}

bool SCMObj::isTrue() const
{
    return !this->isFalse();
}

bool SCMObj::isFalse() const
{
    return this->isBoolean() && (this->b == false);
}

bool SCMObj::isIf() const
{
    return this->isTaggedList("if");
}

bool SCMObj::isAtom() const{
    return this->type != SCMType::Nil &&
    this->type != SCMType::Pair;
}

bool SCMObj::isPair() const{
    return this->type == SCMType::Pair;
}

SCMObj SCMObj::car() const
{
    if(this->isPair()) {
        return *first;
    }
    SCMObj temp;
    temp.setError("CdrError - Expected pair got ", this->toString());
    return temp;
}
    SCMObj car();

bool SCMObj::isTaggedList(const std::string& tag) const{
   return (this->isPair() && this->car().toString() == tag);
}


double SCMObj::getFlonum() const
{
    switch(type) {
    case SCMType::Flonum:
        return flonum;
    case SCMType::Fixnum:
        return fixnum;
    }

}

SCMType SCMObj::getType() const
{
    return this->type;
}

void SCMObj::setString(const std::string& str)
{
    this->isError = false;
    this->str = str;
    this->type = SCMType::String;
}

void SCMObj::setSymbol(const std::string& str)
{
    this->isError = false;
    this->sym = str;
    this->type = SCMType::Symbol;
}

void SCMObj::setFixnum(uint64_t n)
{
    this->isError = false;
    this->fixnum = n;
    this->type = SCMType::Fixnum;
}

void SCMObj::setFlonum(double d)
{
    this->isError = false;
    this->flonum = d;
    this->type = SCMType::Flonum;
}


void SCMObj::setBoolean(bool b)
{
    this->isError = false;
    this->b = b;
    this->type = SCMType::Boolean;
}

void SCMObj::setCharacter(char c)
{
    this->isError = false;
    this->ch = c;
    this->type = SCMType::Character;
}

void SCMObj::setCharacter(const std::string& str)
{
    this->isError = false;
    this->type = SCMType::Character;
    if(str == "space") {
        this->ch = ' ';
    } else if(str == "tab") {
        this->ch = '\t';
    } else if(str == "return") {
        this->ch = '\r';
    } else if(str == "newline" || str == "linefeed") {
        this->ch = '\n';
    } else if(str.size() > 1) {
        this->setError("InvalidChar",str);
    } else {
        this->ch = str[0];
    }
}

void SCMObj::setPair(const SCMObj& first, const SCMObj& rest)
{
    /*
    assert(false);
    this->isError = false;
    pair.first = std::unique_ptr<SCMObj>(other);
    this->type = SCMType::Pair;
    */
    std::stringstream ss;
    ss << std::hex << this;
    setError("Pair not yet implemented", ss.str());
}

void SCMObj::setProcedure(std::function<SCMObj(SCMObj)> fn)
{
    this->isError = false;
    proc = fn;
    this->type = SCMType::Procedure;
}

/*
void SCMObj::setBuiltin(bool b){
    this->isError = false;
    this->builtin = b;
    return *this;
}*/


void SCMObj::setError(const std::string& msg, const std::string& val)
{
    this->errorData.reset(new std::pair<std::string, std::string>());
    this->errorData->first = msg;
    this->errorData->second = val;
    this->isError = true;
}

std::string SCMObj::toString() const
{
    std::stringstream ss;
    if(isError) {
        ss << "Error: " << errorData->first << "\n" << "Value: " << errorData->second;
        return ss.str();
    } else if(type == SCMType::Nil) {
        ss << "nil";
    } else if(isIf()){
        ss << "<Special-Form IF>";
    } else if (type == SCMType::String){
        ss << "\"" << str << "\"";
    } else if(type == SCMType::Symbol){
        ss << sym;
    } else if(type == SCMType::Fixnum){
        ss << fixnum;
    } else if (type == SCMType::Flonum){
        ss << std::setprecision(13) << flonum;
    } else if(type == SCMType::Boolean){
        ss << (b == true? "#t":"#f");
    } else if(type == SCMType::Character){
        std::string prefix = "#\\";
        switch(ch) {
        case '\r':
            ss << prefix << "return";
            break;
        case '\n':
            ss << prefix << "linefeed";
            break;
        case '\t':
            ss << prefix << "tab";
            break;
        case ' ':
            ss << prefix << "space";
            break;
        default:
            ss << prefix << ch;
            break;
        }
    } else if(type == SCMType::Procedure){
        ss << "<";
        ss << "Procedure ";
        //ss << (this->builtin ? "(Builtin) " : "");
        ss << this;
        ss << ">  ";
        ss << proc(*this).toString(); //delete this
    } else if(type == SCMType::Pair){
        ss << "<Pair ";
        ss << this;
        ss << ">";
    }

    std::string t = ss.str();
    return ss.str();
}




std::string SCMObj::getTypeAsString() const
{
    switch(this->type) {
    case SCMType::String:
        return "String";
    case SCMType::Symbol:
        return "Symbol";
    case SCMType::Fixnum:
        return "Fixnum";
    case SCMType::Flonum:
        return "Flonum";
    case SCMType::Boolean:
        return "Boolean";
    case SCMType::Character:
        return "Character";
    case SCMType::Pair:
        return "Pair";
    case SCMType::Procedure:
        return std::string("Procedure ") ;
        //+(this->builtin ? std::string("(Builtin)") : std::string(""));

    }
    return "error";
}

